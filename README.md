# ACIC Labs

**Course:** Applications and Computation for the Internet of Things  
**University:** Instituto Superior Técnico  
**Academic year:** 2019-20

### Team

- 73891 — David Gonçalves  ([david.s.goncalves@tecnico.ulisboa.pt](mailto:david.s.goncalves@tecnico.ulisboa.pt))
- 94112 — Gonçalo Pires ([goncalo.estevinho.pires@tecnico.ulisboa.pt](mailto:goncalo.estevinho.pires@tecnico.ulisboa.pt))

## Lab 1 - Building an embedded system

The goal of this work is to put students for the first time in touch with the Arduino environment to drive simple actuators (in the case LEDs).

**Assignment:** [Lab_1/Assignment.pdf](Lab_1/Assignment.pdf)  
**Report:** [Lab_1/report](Lab_1/report)  
**Tinkercad:** https://www.tinkercad.com/things/dGa2H4E1PCr

**Problems:**

Ohm's law equation (formula): V = I × R  
Power law equation (formula): P = I × V

We applied the power equation incorrectly in the report as we forgot to multiply the current (I) by the volts (V).

## Lab 2 - Sensing the Physical World

The goal of this work is to sense physical quantities and to control actuators according to the measured environment.

**Assignment:** [Lab_2/Assignment.pdf](Lab_2/Assignment.pdf)  
**Report:** [Lab_2/report](Lab_2/report)  
**Tinkercad:** https://www.tinkercad.com/things/iyWLa3pCcJY

**Problems:**

`float` is less efficient than an `int` and could have been avoided.

## Lab 3 - Network of Sensors and Actuators

The goal of this work is the implementation and test of a wired communications link between
two embedded controllers. Communications will be based on the I2C protocol.

**Assignment:** [Lab_3/Assignment.pdf](Lab_3/Assignment.pdf)  
**Report:** [Lab_3/report](Lab_3/report)  
**Tinkercad:** https://www.tinkercad.com/things/hzXe5wznCWx

**Problems:**

For detecting a led fault we have an extra pin (watchdog) for the led with a large resistance (not enough to light up the led) and when we want to check if the led is working we switch that pin for output and the main pin that is usually set to output (for lighting the led) is set to input. Then, we write high and low values to that extra pin. If the main pin reads the high value then the led is not working because it let the current pass.

However, this solution is more complex than it needs to be. A better approach would be to have an extra pin connected to the led and set to input all the time, and when we light the led we'd read the value on that extra pin. If the value is high then the current has passed and the led is at fault.

## Lab 4 - Project: Intelligent control of traffic flow

The goal of this project is the implementation of a modular, automatic and distributed control of traffic lights at intersections. The system is able to adapt the control of the traffic lights to the variations in traffic, to let traffic flow faster on the busiest roads.

The overall system has some degree of fault tolerance and fail safe behavior.

In the laboratory traffic lights will be replaced by LEDs, and traffic sensors by buttons or switches. Otherwise the system to be implemented exhibits realistic behavior. All the peripherals on an intersection are connected to a controller. Intersections exchange information through an I2C bus.

**Assignment:** [Lab_4/Assignment.pdf](Lab_4/Assignment.pdf)  
**Report:** [Lab_4/report/Report.pdf](Lab_4/report/Report.pdf)  
**Global report:** [Lab_4/report/global_report.pdf](Lab_4/report/global_report.pdf)  
**Tinkercad:** https://www.tinkercad.com/things/k7mZ8TXh4Iq

**Problems:**

Not extensively tested and reviewed. May have bugs or design problems.