const int GREEN = 13;
const int RED = 12;
const int BLUE = 11;
const int YELLOW = 10;

/**
 * External Interrupts: 2 and 3. These pins can be configured to
 * trigger an interrupt on a low value, a rising or falling edge,
 * or a change in value. See the attachInterrupt() function for details.
 * https://www.arduino.cc/en/reference/board
 */
const int BUTTON = 2;

const int delayMs = 1000;

/**
 * You should declare as volatile any variables that you modify
 * within the attached function.
 * https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
 */
volatile bool pause = false;

void switchLed(int ledOn, int ledOff, int milliseconds) {
	while (pause) {
		delay(50);
	}
	if (ledOff >= 0 && ledOff <= 13) {
		digitalWrite(ledOff, LOW);
	}
	if (ledOn >= 0 && ledOn <= 13) {
		digitalWrite(ledOn, HIGH);
	}
	delay(milliseconds);
}

/// Put your setup code here, to run once:
void setup() {
	pinMode(GREEN, OUTPUT); // sets the digital pin as output, it is input by default
	pinMode(RED, OUTPUT);
	pinMode(BLUE, OUTPUT);
	pinMode(YELLOW, OUTPUT);

	attachInterrupt(digitalPinToInterrupt(BUTTON), [](){pause=!pause;}, FALLING);
}

/// put your main code here, to run repeatedly:
void loop() {
	switchLed(GREEN, YELLOW, delayMs);
	switchLed(RED, GREEN, delayMs);
	switchLed(BLUE, RED, delayMs);
	switchLed(YELLOW, BLUE, delayMs);
	switchLed(-1, YELLOW, delayMs);
}
