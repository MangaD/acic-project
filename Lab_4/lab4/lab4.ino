#include <Wire.h>

#define GREEN_PIN_W 11
#define YELLOW_PIN_W 10
#define RED_PIN_W 9
#define RED_PIN_FAULT_W 13

#define GREEN_PIN_S 5
#define YELLOW_PIN_S 6
#define RED_PIN_S 7
#define RED_PIN_FAULT_S 12

// Induction loops
#define W_BUTTON_PIN 3
#define S_BUTTON_PIN 2

/**
 * The identification of the intersection is specified by its (x, y)
 * coordinates in space. These are setup by 3 × 2 wire jumpers
 * (wires connected to VCC or GND) connected to input ports of the controller:
 * x = a2, a1, a0 (3 bits)
 * y = digital pin, digital pin, a3 (3 bits)
 */
// Intersection coordinates
#define X0_PIN A0
#define X1_PIN A1
#define X2_PIN A2

#define Y0_PIN A3
#define Y1_PIN 8
#define Y2_PIN 4

// Each semaphore's orientation: 0-N, 1-S, 2-E, 3-W
#define W_PIN 0
#define S_PIN 1

// Time
const unsigned long initialDelayMs = 6*1000;
unsigned long cyclePeriod = 20*1000;

// True if start with green for west
bool startWest = true;

struct Cars {
	byte cars_N = 0;
	byte cars_S = 0;
	byte cars_E = 0;
	byte cars_W = 0;
};

// Forward declarations
void mode0();
void mode1();
void mode2();
void setupCoord();
void switchTraffic();
void sendToAll(byte event1, byte event2, int nCars1, int nCars2, long t);
void turnOffAllRedAndGreenLeds();
void turnOffAllLeds();
void readValues(int i);
bool comesFromAdjacentIntersection(byte event, byte origin);
bool hasMaxFlow(Cars c);
void adjustPhase(byte event);
int roundHundredth(int n);
void debug(const char * msg, float val);

// Mode to execute, can be 'mode0', 'mode1' and 'mode2'
void (*mode)() = mode2;

// Time car takes to travel from one intersection to another
const unsigned long timeToTravel = 6000;

long last_R2GW = 0;
long next_R2GW = 0;

long last_R2GS = 0;
long next_R2GS = 0;

unsigned long nCarsW = 0;
unsigned long nCarsS = 0;

unsigned long nCarsW2 = 0;
unsigned long nCarsS2 = 0;

// Intersection information, values [0,3]
// 0-N, 1-S, 2-E, 3-W
int ori_W;
int ori_S;
int coord_X;
int coord_Y;
byte id;
byte neighbors[4]; // 0-up, 1-down, 2-left, 3-right

int ori_max_flow;// Orientation of semaphore with most flow
long t_recv = 0;

// LEDS
class led {

private:
	const int pin;
	const int watchdog_pin;
	unsigned long previousBlinkMs;

public:
	explicit led(int pin, int watchdog_pin = -1) :
		pin(pin), watchdog_pin(watchdog_pin), previousBlinkMs(0) {

		if (pin >= 0 && pin <= 13) {
			pinMode(pin, OUTPUT);
		}
	}

	void blink(const unsigned long blinkIntervalMs) {
		unsigned long timeMs = millis()+1;// don't let previousBlinkMs be 0 twice

		if (previousBlinkMs == 0 || (timeMs - previousBlinkMs >= blinkIntervalMs)) {
			previousBlinkMs = timeMs;
			if (pin >= 0 && pin <= 13) {
				digitalWrite(pin, !digitalRead(pin));
			}
		}
	}

	bool isLedMissing() {

		if (pin < 0 || pin > 13 || watchdog_pin < 0 || watchdog_pin > 13) {
			return false;
		}

		bool result = false;
		const int ledState = digitalRead(pin);

		pinMode(pin, INPUT);
		pinMode(watchdog_pin, OUTPUT);

		digitalWrite(watchdog_pin, HIGH);
		const int readA = digitalRead(pin);

		digitalWrite(watchdog_pin, LOW);
		const int readB = digitalRead(pin);

		if (readA == HIGH && readB == LOW) {
			result = true;
			debug("Led has disconnected. Pin: ", pin);
		}

		pinMode(watchdog_pin, INPUT);
		pinMode(pin, OUTPUT);

		// Restore the led's output
		digitalWrite(pin, ledState);
		return result;
	}

	void turnOn() {
		if (pin >= 0 && pin <= 13) {
			digitalWrite(pin, HIGH);
		}
	}

	void turnOff() {
		if (pin >= 0 && pin <= 13) {
			digitalWrite(pin, LOW);
		}
	}
};

// led (pin, watchdog_pin)
led W_GREEN (GREEN_PIN_W);
led W_YELLOW (YELLOW_PIN_W);
led W_RED (RED_PIN_W, RED_PIN_FAULT_W);

led S_GREEN (GREEN_PIN_S);
led S_YELLOW (YELLOW_PIN_S);
led S_RED (RED_PIN_S, RED_PIN_FAULT_S);

// DATA INTERCHANGE
union Time {
	unsigned long longNumber;
	byte longBytes[4];
};

struct Message {
	/*
	 * (x, y) identifier of the intersection to which the frame is sent –
	 * (0, a2, a1, a0, 0, b2, b1, b0), 4 bits field for x + 4 bits field for y.
	 */
	byte destination;
	byte source;

	/*
	 * 0 – R2G_N (red to green in the North access);
	 * 1 – R2G_S (red to green in the South access);
	 * 2 – R2G_E (red to green in the East access);
	 * 3 – R2G W (red to green in the West access);
	 * n – (n > 3, reserved).
	 */
	byte event;
	Cars cars;

	//Time (32 bits integer): Time stamp associated with the event. The time unit is 100 ms.
	Time time;

	Message() {}

	Message(byte dest, byte event, Cars c, long t) {
		this->destination = dest;
		this->source = id;
		this->event = event;
		this->cars = c;
		this->time.longNumber = roundHundredth(t);
	}

	void send() {
		// debug("Message sent to: ", this->destination);
		// this->print();
		// Serial.println(this->time.longNumber);
		// Serial.println((this->time.longNumber-8000) % 20000);
		Wire.beginTransmission(this->destination);
		Wire.write((byte *)this, sizeof *this);
		Wire.endTransmission();
	}

	void parse() {
		if (this->destination != id) return; // don't do anything for messages that are not for us
		if (!hasMaxFlow(this->cars)) return;
		if (!comesFromAdjacentIntersection(this->event, this->source)) return;
		if (this->event != ori_max_flow) {
			Serial.println("Event opposed to max flow!");
			return;
		}
		Serial.println("Event correct!");
		adjustPhase(this->event);
	}

	void print() {
		debug("Destination: ", this->destination);
		debug("Source: ", this->source);
		debug("Event: ", this->event);
		debug("cars_N: ", this->cars.cars_N);
		debug("cars_S: ", this->cars.cars_S);
		debug("cars_E: ", this->cars.cars_E);
		debug("cars_W: ", this->cars.cars_W);
		debug("time: ", this->time.longNumber);
		Serial.println("----------------------------");
	}
};

void setup() {
	Serial.begin(9600);
	Wire.onReceive(readValues);
	attachInterrupt(digitalPinToInterrupt(W_BUTTON_PIN),[]() { nCarsW++; }, RISING);
	attachInterrupt(digitalPinToInterrupt(S_BUTTON_PIN),[]() { nCarsS++; }, RISING);
	setupCoord();
	Wire.begin(id);
	findNeighbors();
	debug("Coordinates: ", id);
}

void setupCoord() {

	// 0-N, 1-S, 2-E, 3-W
	ori_W = digitalRead(W_PIN);
	ori_S = digitalRead(S_PIN);

	debug("W: ", ori_W);
	debug("S: ", ori_S);

	const int x0 = (analogRead(X0_PIN) > 0 ? 1 : 0);
	const int x1 = (analogRead(X1_PIN) > 0 ? 1 : 0);
	const int x2 = (analogRead(X2_PIN) > 0 ? 1 : 0);

	const int y0 = (analogRead(Y0_PIN) > 0 ? 1 : 0);
	const int y1 = digitalRead(Y1_PIN);
	const int y2 = digitalRead(Y2_PIN);

	coord_X = (x0 + (x1 << 1) + (x2 << 2));
	coord_Y = (y0 + (y1 << 1) + (y2 << 2));

	debug("X: ", coord_X);
	debug("Y: ", coord_Y);

	id = (coord_X << 4) + coord_Y;
}

void readValues(int i) {
	if (mode == mode2) {
		Message msg;
		while (Wire.available() > 0) {
			msg.destination = Wire.read();
			msg.source = Wire.read();
			msg.event = Wire.read();
			msg.cars.cars_N = Wire.read();
			msg.cars.cars_S = Wire.read();
			msg.cars.cars_E = Wire.read();
			msg.cars.cars_W = Wire.read();
			msg.time.longBytes[0] = Wire.read();
			msg.time.longBytes[1] = Wire.read();
			msg.time.longBytes[2] = Wire.read();
			msg.time.longBytes[3] = Wire.read();
		}
		t_recv = roundHundredth(millis());
		msg.parse();
	}
}

// Returns true if the information comes from an intersection that should
// influence this intersection's behavior
bool comesFromAdjacentIntersection(byte event, byte origin) {
	// 16d = 10000b
	if (origin == id+16 && event == 2) { return true; } // If msg came from the right and event corresponds to cars coming from the East
	else if (origin == id-16 && event == 3) { return true; } // If msg came from the left and event corresponds to cars coming from the West
	else if (origin == id+1 && event == 0) { return true; } // If msg came from the top and event corresponds to cars coming from the North
	else if (origin == id-1 && event == 1) { return true; } // If msg came from below and event corresponds to cars coming from the South
	return false;
}

bool hasMaxFlow(Cars c) {
	int cars_list[4];
	cars_list[0] = c.cars_N;
	cars_list[1] = c.cars_S;
	cars_list[2] = c.cars_E;
	cars_list[3] = c.cars_W;

	int max_cars = cars_list[ori_max_flow];
	for (int i = 0; i < 4; i++) {
		if (i != ori_max_flow && cars_list[i] >= max_cars) {
			return false;
		}
	}
	return true;
}

void adjustPhase(byte event) {

	static bool toDelay = false;
	static long finalDelay;

	long last = 0;
	long next = 0;

	if (event == ori_W) {
		last = last_R2GW;
		next = next_R2GW;
	} else {
		last = last_R2GS;
		next = next_R2GS;
	}

	//debug("A3: ", last);
	//debug("A2: ", next);
	//debug("A1: " , t_recv);

	long delay1 = (t_recv + 6000) - last;
	long delay2 = (t_recv + 6000) - next;

	//debug("delay1: ", delay1);
	//debug("delay2: ", delay2);

	if (abs(delay1) < 50 || abs(delay2) < 50) {
		Serial.println("Not necessary to adjust!");
		return;
	}

	finalDelay = (abs(delay1) > abs(delay2) ? delay2 : delay1);
	toDelay = (finalDelay < 0 ? true : false);

	//debug("toDelay: ", toDelay);
	debug("Final delay: ", finalDelay);

	if (toDelay) {
		if (abs(finalDelay) >= 2000) {
			cyclePeriod = cyclePeriod - 2000;
		} else {
			cyclePeriod = cyclePeriod - abs(finalDelay);
		}
	} else {
		if (finalDelay >= 2000) {
			cyclePeriod = cyclePeriod + 2000;
		} else {
			cyclePeriod = cyclePeriod + finalDelay;
		}
	}

	debug("New cyclePeriod: ", cyclePeriod);
}

void loop() {
	if (millis() <= initialDelayMs || W_RED.isLedMissing() || S_RED.isLedMissing()) {
		turnOffAllRedAndGreenLeds();
		W_YELLOW.blink(2000);
		S_YELLOW.blink(2000);
	} else {
		mode();
	}

	delay(50);
}

void mode0() {
	static unsigned long switchAt = cyclePeriod * 0.5;

	static unsigned long lastSwitch = 0;
	unsigned long timeMs = millis() + 1; // don't let lastSwitch be 0 twice

	if (lastSwitch == 0 || (timeMs - lastSwitch >= switchAt)) {
		lastSwitch = timeMs;
		switchTraffic();
	}
}

void mode1() {
	modo();
}

void mode2() {
	modo();
}

void modo() {

	static unsigned long switchAt = cyclePeriod * 0.5;

	static unsigned long cycles = 0;
	static unsigned long timesSwitched = 0;
	static unsigned long lastCycle = initialDelayMs;
	static unsigned long lastSwitch = 0;
	unsigned long timeMs = millis() + 1;

	if (timeMs - lastCycle >= cyclePeriod) {
		cycles++;
		timesSwitched = 0;
		lastSwitch = 0;
		lastCycle = timeMs;

		if (nCarsS + nCarsW > 0) {
			double dutyCycle = (double) (startWest ? nCarsW : nCarsS) / (nCarsS + nCarsW);
			dutyCycle = (dutyCycle < 0.25 ? 0.25 : dutyCycle);
			dutyCycle = (dutyCycle > 0.75 ? 0.75 : dutyCycle);
			switchAt = cyclePeriod * dutyCycle;
		} else {
			switchAt = cyclePeriod * 0.5;
		}

		// always has the direction of max flow in each period
		ori_max_flow = (nCarsW >= nCarsS ? ori_W : ori_S);

		debug("Max flow ori: " , ori_max_flow);
		debug("W cars: ", nCarsW);
		debug("S cars: ", nCarsS);

		nCarsW2 = nCarsW;
		nCarsS2 = nCarsS;
		nCarsW = nCarsS = 0;
		cyclePeriod = 20*1000;// restore cyclePeriod
	}

	// from 10 to 10 secs
	if (lastSwitch == 0 || (timesSwitched < 2 && timeMs - lastSwitch >= switchAt)) {
		timesSwitched++;
		lastSwitch = timeMs;
		switchTraffic();
	}
}

void switchTraffic() {

	static bool firstExec = true;
	static bool westPass = (startWest ? false : true);
	westPass = !westPass;

	if (mode == mode2) {
		last_R2GW = roundHundredth(millis());
		next_R2GW = last_R2GW + cyclePeriod;
		if (westPass) {
			sendToAll(ori_W, ori_S, nCarsW2, nCarsS2, millis());
		} else {
			sendToAll(ori_S, ori_W, nCarsS2, nCarsW2, millis());
			nCarsW2 = 0;
			nCarsS2 = 0;
		}
	}

	if (!firstExec) {
		turnOffAllLeds();
		S_YELLOW.turnOn();
		W_YELLOW.turnOn();
		delay(1000);
	}

	firstExec = false;
	turnOffAllLeds();

	if (westPass) {
		S_RED.turnOn();
		W_GREEN.turnOn();
	} else {
		W_RED.turnOn();
		S_GREEN.turnOn();
	}
}

void sendToAll(byte event1, byte event2, int nCars1, int nCars2, long t) {
	Cars c;
	if (event1 == 0) { c.cars_N = nCars1; }
	else if (event1 == 1) { c.cars_S = nCars1; }
	else if (event1 == 2) { c.cars_E = nCars1; }
	else if (event1 == 3) { c.cars_W = nCars1; }

	if (event2 == 0) { c.cars_N = nCars2; }
	else if (event2 == 1) { c.cars_S = nCars2; }
	else if (event2 == 2) { c.cars_E = nCars2; }
	else if (event2 == 3) { c.cars_W = nCars2; }

	for (int i = 0; i < 4; i++) {
		Message m(neighbors[i], event1, c, t);
		if (m.destination != id) {
			m.send();
		}
	}
}

void findNeighbors() {
	// x and y must not be negative
	neighbors[0] = (coord_X << 4) + (coord_Y + (coord_Y == 2 ? 0 : 1)); // up
	neighbors[1] = (coord_X << 4) + (coord_Y - (coord_Y == 0 ? 0 : 1)); // down
	neighbors[2] = ((coord_X - (coord_X == 0 ? 0 : 1)) << 4) + (coord_Y); // left
	neighbors[3] = ((coord_X + (coord_X == 2 ? 0 : 1)) << 4) + (coord_Y); // right
}

void turnOffAllLeds() {
	W_GREEN.turnOff();
	W_YELLOW.turnOff();
	W_RED.turnOff();
	S_GREEN.turnOff();
	S_YELLOW.turnOff();
	S_RED.turnOff();
}

void turnOffAllRedAndGreenLeds() {
	W_GREEN.turnOff();
	W_RED.turnOff();
	S_GREEN.turnOff();
	S_RED.turnOff();
}

int roundHundredth(int n) {
	n = n + 49;
	return n - (n % 100);
}

void debug(const char * msg, float val) {
	Serial.print(msg);
	Serial.println(val);
}
