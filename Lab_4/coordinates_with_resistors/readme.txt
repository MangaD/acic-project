Tens duas resistencias em que uma é o dobro da outra (no tinkerpad usei exatamento
o dobro, mas como nao temos resistencias com exatamente esses valores , tive que
fazer aquelas contas, mas vou explicar como se fossem exatamente o dobro uma da
outra). A resistencia com menor valor (A)  representa o bit mais significativo
enquanto a de maior valor (B) representa o bit menos significativo. Se a porta
analogica onde ligas as resistencias ler valores de 0 a 1000, para o valor ser
maior que 500, 'A' tem de estar ligada ao 5v

se A e B estiverem ligadas ao 0 a porta analogica lê 0, se A =0 e B = 1, entao lê 250
os valores nao teem de ser exactos, se a porta ler 750 ( 500 do A + 250 do B)
significa que as 2 resistencias estao ligadas ao 5V
ou seja entre 750 e 1000 é a mesma coisa

[0,250[ --> 0 , [250,500[ --> 1 , [500,750[ --> 2 , [750, 1000] --> 3
 
o facto de as resistencias serem o dobro uma da outra sabes que uma anda de 500 em
500 (resistencia de menor valor ) e outra de 250 em 250 (resitencia de maior valor)

---

os valores esquisitos na funçao setupCord() foi para as contas baterem certo,
pq nao temos uma resistencia exatamente o dobro de outra

as resistencias que usei para calcular as cordenadas foram de 4,7k e 10k ,
convem usar estes valores para bater certo com os calculos

depois podes ligar os 2 arduinos aí, nao precisas de replicar isso para o outro

---

MangaDToday at 5:20 PM
"as resistencias que usei para calcular as cordenadas foram de 4,7k e 10k ,
convem usar estes valores para bater certo com os calculos"
são 3 resistências para cada coordenada, usaste 10k para o bit menos significativo
e 4.7k para os outros 2?
 
goncaloToday at 5:21 PM
usei so 2 resistencias para cada cordenada
pq so é preciso 2, e pq para usar 3 resistencias complicava bastante os calculos pq nao existem resistencias com relacaçao 1R --> 2R --> 4R
o stor disse que podemos mudar algumas coisas
mas quem faz para 2 tb faz para 3
a logica é a mesma
mas para aqui é desnecessario

---

a funçao readValues e imprimir_msg foi mais para testar
os endereços de cada arduino sao iguais ao source, que é (x<<4)+y, assim cada
arduino sabe os endereços dos seus vizinhos e nao é preciso andar a declarar variaveis

é so replicares a parte da esquerda ( 1 botao + 3 leds + policia).  A parte da direita
sao as 4 possiveis combinaçoes das resistencias para as cordenadas (A2 e A3) e orientacao,
que nao liguei aqui. Se A2 for o x e A3 o y, as cordenadas deste arduino ficam x=2 e y=0

a parte da direita é comum aos 2 arduinos, fiz isto assim pq poupa-se em resistencias
e é mais facil mudar os fios do que as resistencias quando é preciso trocar as cordenadas
isto na pratica da para juntar mais as peças, no tinkerpad é que é limitado, mas
basicamente tenho 4 partes da esquerda + a parte da direita

os verdes sao os A2, A3, A1

