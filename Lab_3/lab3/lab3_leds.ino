// Slave Arduino

#include <Wire.h>

// LEDS
const int YELLOW = 5;
const int GREEN = 4;
// On Arduino Uno, the PWM pins are 3, 5, 6, 9, 10 and 11
const int RED = 3;

// State
bool yellowState = LOW;
bool greenState = LOW;
int redState = 0;

union long_bytes
{
	unsigned long value;
	char bytes[4];
};

void setup() {
	Serial.begin(9600);

	Wire.begin(8);
	Wire.onReceive(readValues);

	pinMode(YELLOW, OUTPUT);
	pinMode(GREEN, OUTPUT);
	pinMode(RED, OUTPUT);
}

void loop() {
	digitalWrite(YELLOW, yellowState);
	digitalWrite(GREEN, greenState);
	analogWrite(RED, redState);
}

void readValues(int i) {
	while (Wire.available() > 0) {
		long_bytes ms;
		ms.bytes[0] = Wire.read();
		ms.bytes[1] = Wire.read();
		ms.bytes[2] = Wire.read();
		ms.bytes[3] = Wire.read();
		yellowState = Wire.read();
		greenState = Wire.read();
		redState = Wire.read();
		debug("Latency: ", millis() - ms.value);
	}
}

void debug(const char * msg, float val) {
	Serial.print(msg);
	Serial.println(val);
}
