// Master Arduino

#include <Wire.h>

// Sensors
const int tempSensor = A0;
const int potentSensor = A1;
const int lightSensor = A2;

// Thresholds
const float tempThreshold = 30.0f;
const int lowerBlinkMs = 200;
const int higherBlinkMs = 2000;

// State
bool yellowState = LOW;
bool greenState = LOW;
int redState = 0;

// Calibration
struct {
	int min = 0;
	int max = 1023;
} light, potent;

union long_bytes
{
	unsigned long value;
	char bytes[4];
};

void setup() {
	Serial.begin(9600);

	Wire.begin();

	calibratePotent();
	calibrateLight();
}

void loop() {
	controlTemp();
	controlPotent();
	controlLight();

	// Transmission to port 8 of the I2C bus
	long_bytes ms;
	ms.value = millis();
	Wire.beginTransmission(8);
	Wire.write(ms.bytes, 4); // For calculating latency
	Wire.write(yellowState);
	Wire.write(greenState);
	Wire.write(redState);
	Wire.endTransmission();
}

void calibrateLight() {
	while (millis() < 1000) {
		int lightValue = analogRead(lightSensor);
		light.max = (lightValue > light.max ? lightValue : light.max);
		light.min = (lightValue < light.min ? lightValue : light.min);
	}
}

void calibratePotent() {
	while (millis() < 500) {
		int potentValue = analogRead(potentSensor);
		potent.max = (potentValue > potent.max ? potentValue : potent.max);
		potent.min = (potentValue < potent.min ? potentValue : potent.min);
	}
}

float readTemp(const int ts) {
	float temp = analogRead(ts);
	return (((temp / 1024.0f) * 5.0f) - 0.5f) * 100.0f;
}

float readPotent(const int ps) {
	int val = map(analogRead(ps), potent.min, potent.max, lowerBlinkMs, higherBlinkMs);
	return constrain(val, lowerBlinkMs, higherBlinkMs);
}

void controlTemp() {
	float tvalue = readTemp(tempSensor);

	yellowState = (tvalue > tempThreshold ? HIGH : LOW);

	debug("Temperature: ", tvalue);
}

void controlPotent() {
	static unsigned long previousBlinkMs = 0;

	unsigned long timeMs = millis();

	int intervalMs = readPotent(potentSensor);
	
	if (timeMs - previousBlinkMs >= intervalMs) {
		previousBlinkMs = timeMs;
		greenState = !greenState;
	}

	debug("Potentiometer Min: ", potent.min);
	debug("Potentiometer Max: ", potent.max);
	debug("Potentiometer: ", intervalMs);
}

void controlLight() {
	int val = analogRead(lightSensor);

	int pwm = map(val, light.min, light.max, 255, 0);
	pwm = constrain(pwm, 0, 255);

	redState = pwm;

	debug("Light Min: ", light.min);
	debug("Light Max: ", light.max);
	debug("Light PWM: ", pwm);
}

void debug(const char * msg, float val) {
	Serial.print(msg);
	Serial.println(val);
}
