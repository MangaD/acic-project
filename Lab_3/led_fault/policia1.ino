const int lightSensor = A3;
const int RED = 3;
const int policia = 2;

bool estado = LOW;

unsigned long previousBlinkMs = 0;

void setup(){
	Serial.begin(9600);
	pinMode(policia, INPUT); 
	pinMode(RED, OUTPUT);
	pinMode(lightSensor,INPUT);
}

void loop(){

	controlLight();

	unsigned long timeMs = millis();
	if (timeMs - previousBlinkMs >= 1000) {
		previousBlinkMs = timeMs;
		verificarPins(); 

	}


}

void verificarPins(){
	pinMode(policia, OUTPUT);
	pinMode(RED, INPUT);

	digitalWrite(policia, HIGH);
	int valor1 = digitalRead(RED);

	digitalWrite(policia, LOW);
	int valor2 = digitalRead(RED);

	if(valor1 == HIGH && valor2 == LOW){
		Serial.println("led disconectado");
	}else{
		Serial.println("tudo ok");
	}

	pinMode(policia, INPUT);
	pinMode(RED, OUTPUT);

}


void controlLight() {
	int val = analogRead(lightSensor);

	int pwm = map(val, 0, 700, 255, 0);
	pwm = constrain(pwm, 0, 255);

	analogWrite(RED, pwm);
	//debug("Light PWM: ", pwm);
}


void debug(const char * msg, float val) {
	Serial.print(msg);
	Serial.println(val);
}
