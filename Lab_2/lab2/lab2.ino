// LEDS
const int YELLOW = 5;
const int GREEN = 4;
// On Arduino Uno, the PWM pins are 3, 5, 6, 9, 10 and 11
const int RED = 3;

// Sensors
const int tempSensor = A0;
const int potentSensor = A1;
const int lightSensor = A2;

// Thresholds
const float tempThreshold = 30.0f;
const int lowerBlinkMs = 200;
const int higherBlinkMs = 2000;

// Calibration
struct {
	int min = 0;
	int max = 1023;
} light, potent;

void setup() {
	Serial.begin(9600);

	pinMode(YELLOW, OUTPUT);
	pinMode(GREEN, OUTPUT);
	pinMode(RED, OUTPUT);

	calibratePotent();
	calibrateLight();
}

void loop() {
	controlTemp();
	controlPotent();
	controlLight();
}

void calibrateLight() {
	while (millis() < 10000) {
		int lightValue = analogRead(lightSensor);
		light.max = (lightValue > light.max ? lightValue : light.max);
		light.min = (lightValue < light.min ? lightValue : light.min);
	}
}

void calibratePotent() {
	while (millis() < 5000) {
		int potentValue = analogRead(potentSensor);
		potent.max = (potentValue > potent.max ? potentValue : potent.max);
		potent.min = (potentValue < potent.min ? potentValue : potent.min);
	}
}

float readTemp(const int ts) {
	float temp = analogRead(ts);
	return (((temp / 1024.0f) * 5.0f) - 0.5f) * 100.0f;
}

float readPotent(const int ps) {
	int val = map(analogRead(ps), potent.min, potent.max, lowerBlinkMs, higherBlinkMs);
	return constrain(val, lowerBlinkMs, higherBlinkMs);
}

void controlTemp() {
	float tvalue = readTemp(tempSensor);

	digitalWrite(YELLOW, (tvalue > tempThreshold ? HIGH : LOW));

	debug("Temperature: ", tvalue);
}

void controlPotent() {
	static unsigned long previousBlinkMs = 0;
	static bool ledState = LOW;

	unsigned long timeMs = millis();

	int intervalMs = readPotent(potentSensor);
	
	if (timeMs - previousBlinkMs >= intervalMs) {
		previousBlinkMs = timeMs;
		ledState = !ledState;
		digitalWrite(GREEN, ledState);
	}

	debug("Potentiometer Min: ", potent.min);
	debug("Potentiometer Max: ", potent.max);
	debug("Potentiometer: ", intervalMs);
}

void controlLight() {
	int val = analogRead(lightSensor);

	int pwm = map(val, light.min, light.max, 255, 0);
	pwm = constrain(pwm, 0, 255);

	/**
	 * analogWrite(0) means a signal of 0% duty cycle.
	 * analogWrite(127) means a signal of 50% duty cycle.
	 * analogWrite(255) means a signal of 100% duty cycle.
	 * https://create.arduino.cc/projecthub/muhammad-aqib/arduino-pwm-tutorial-ae9d71
	 */
	analogWrite(RED, pwm);

	debug("Light Min: ", light.min);
	debug("Light Max: ", light.max);
	debug("Light PWM: ", pwm);
}

void debug(const char * msg, float val) {
	Serial.print(msg);
	Serial.println(val);
}
